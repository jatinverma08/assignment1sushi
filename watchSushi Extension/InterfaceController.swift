//
//  InterfaceController.swift
//  watchSushi Extension
//
//  Created by jatin verma on 2019-11-01.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController , WCSessionDelegate{
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
  
    @IBAction func right() {
        
        

                  if (WCSession.default.isReachable) {
                    print("Watch button is pressed")
                           
                           // the message is in dictionary
                    let message = ["Action ":"right"] as [String : Any]
                          // send the message to the watch
                        
                    WCSession.default.sendMessage(message, replyHandler: nil)
                    

                      }
                
        
    }
    @IBAction func left() {
        if (WCSession.default.isReachable) {
                           print("Watch button is pressed")
                                  
                                  // the message is in dictionary
                           let message = ["Action ":"left"] as [String : Any]
                                 // send the message to the watch
                               
                           WCSession.default.sendMessage(message, replyHandler: nil)
                           
                           
              
                             }
                       
    }
    
    @IBOutlet weak var nameLabel: WKInterfaceLabel!
    @IBAction func name() {
        print("select name button pressed")
               // label update
               
               print("name button pressed")
                      let cannedResponses = ["Jay"]
                      presentTextInputController(withSuggestions: cannedResponses, allowedInputMode: .plain) {
                          
                          (results) in
                          
                          if (results != nil && results!.count > 0) {
                              // 2. write your code to process the person's response
                              let userResponse = results?.first as? String
                              print(userResponse)
                              self.nameLabel.setText(userResponse)

                                   // the message is in dictionary
                            let message = ["Name":self.nameLabel] as [String : Any]
                                  // send the message to the watch
                                
                            WCSession.default.sendMessage(message, replyHandler: nil)
                            
                          }
                      }
               
            
    }
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
   if WCSession.isSupported() {
           print("Watch: Watch supports WatchConnectivity!")
                  let session = WCSession.default
                  session.delegate = self
                  session.activate()
              }
       else {
       print("Watch: Watch does not support WatchConnectivity")
   }
    
    
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
